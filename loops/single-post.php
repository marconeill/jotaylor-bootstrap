<?php
/**!
 * The Single Posts Loop
 */
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
    <header class="mb-4">
      <h1>
        <?php the_title()?>
      </h1>
      <span>Share</span>
      <div class="ssk-xs ssk-group">
    <a href="" class="ssk ssk-facebook"></a>
    <a href="" class="ssk ssk-twitter"></a>
    <a href="" class="ssk ssk-pinterest"></a>
</div>
    </header>
    <main>
      <?php
        the_post_thumbnail();
        the_content();
        wp_link_pages();
      ?>
    </main>
  </article>
<?php
    if ( comments_open() || get_comments_number() ) :
		endif;
  endwhile; else:
    wp_redirect(esc_url( home_url() ) . '/404', 404);
    exit;
  endif;
?>
<div class="row mt-5 border-top pt-3">
  <div class="col">
    <?php previous_post_link('%link', '<i class="fas fa-fw fa-arrow-left"></i> Previous post: '.'%title'); ?>
  </div>
  <div class="col text-right">
    <?php next_post_link('%link', 'Next post: '.'%title' . ' <i class="fas fa-fw fa-arrow-right"></i>'); ?>
  </div>
</div>
