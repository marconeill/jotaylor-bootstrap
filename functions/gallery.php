<?php
/**!
 * Author Bio
 */

 add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );

 function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
     $my_attr = 'interestedin';

     if ( isset( $atts[$my_attr] ) ) {
         $out[$my_attr] = $atts[$my_attr];
     }

     return $out;
 }


 add_filter('post_gallery', 'my_post_gallery', 10, 2);
 function my_post_gallery($output, $attr) {
     global $post;

     if (isset($attr['orderby'])) {
         $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
         if (!$attr['orderby'])
             unset($attr['orderby']);
     }

     extract(shortcode_atts(array(
         'order' => 'ASC',
         'orderby' => 'menu_order ID',
         'id' => $post->ID,
         'itemtag' => 'dl',
         'icontag' => 'dt',
         'captiontag' => 'dd',
         'columns' => 3,
         'size' => 'thumbnail',
         'include' => '',
         'exclude' => ''
     ), $attr));

     $id = intval($id);
     if ('RAND' == $order) $orderby = 'none';

     if (!empty($include)) {
         $include = preg_replace('/[^0-9,]+/', '', $include);
         $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

         $attachments = array();
         foreach ($_attachments as $key => $val) {
             $attachments[$val->ID] = $_attachments[$key];
         }
     }

     if (empty($attachments)) return '';

     // Here's your actual output, you may customize it to your need
     $output = "<div class=\"gallery gallery-columns-3 gallery-size-thumbnail\">\n";

     // Now you loop through each attachment
     foreach ($attachments as $id => $attachment) {
         // Fetch the thumbnail (or full image, it's up to you)
 //      $img = wp_get_attachment_image_src($id, 'medium');
 //      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
         $img = wp_get_attachment_image_src($id, 'medium');
         $imglrg = wp_get_attachment_image_src($id, 'large');
         $captiontitle = wptexturize($attachment->post_excerpt);
         $enquireform = do_shortcode( '[contact-form-7 id="196" title="Contact form 1" interestedin="' . $captiontitle . '"]' );

        $output .= "<dl class=\"gallery-item\">\n";
        $output .= "<dt class=\"gallery-icon\">\n";
        $output .= "<img class=\"attachment-thumbnail size-thumbnail\" src=\"{$img[0]}\" width=\"{$img[1]}\" height=\"{$img[2]}\" alt=\"\" />\n";

        if ( $captiontag && trim($attachment->post_excerpt) ) {
          $output .= "
            <{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'><strong>
            " . wptexturize($attachment->post_excerpt) . "
            </strong></{$captiontag}>";}

        if ( $captiontag && trim($attachment->post_content) ) {
            $output .= "
                <{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
                " . wptexturize($attachment->post_content) . "
                </{$captiontag}>";}


        $output .= "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#{$img[0]}\">
          Preview
        </button>\n";


        $output .= "<div class=\"modal fade\" id=\"{$img[0]}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
          <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
              <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel\">{$captiontitle}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                  <span aria-hidden=\"true\">&times;</span>
                </button>
              </div>
              <div class=\"modal-body\">
              <img src=\"{$imglrg[0]};\" />

              </div>
            </div>
          </div>
        </div>
\n";


$output .= "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#{$img[1]}\">
  Enquire
</button>\n";


$output .= "<div class=\"modal fade\" id=\"{$img[1]}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Enquiry - <strong>{$captiontitle}</strong></h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body row\">
      {$enquireform}
      </div>
    </div>
  </div>
</div>
\n";

        $output .= "</dt>\n";
        $output .= "</dl>\n";
     }

     $output .= "</div>\n";

     return $output;
 }
